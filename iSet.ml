(*
 * ISet - Interval sets
 * Copyright (C) 1996-2003 Xavier Leroy, Nicolas Cannasse, Markus Mottl,
 * Jacek Chrzaszcz, Tomasz Necio
 * Code review: Julia Bazińska
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version,
 * with the special exception on linking described in file LICENSE.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *)

(* * * * TYPES * * * *)

(* we represent intervals as pairs of ints: (int * int) *)

type position_in_regard_to_interval = 
    OnlyElement | Below | LowerEnd | StrictlyInside | HigherEnd | Above

type t =
    (* a set of integer intervals, implemented as AVL tree;
       the values in nodes are: left subtree, root, right subtree,
       tree height, number of elements within the tree *)
    Empty | Node of t * (int * int) * t * int * int

(* * * * INTERVAL FUNCTIONS * * * *)

let invalid_integral = (1, 0) 
(* for use when an integral must be given but doesn't exist *)

let pos_interval x (a, b) =
    if (a = b) && (x = a) then OnlyElement
    else if a > x then Below
    else if a = x then LowerEnd
    else if (a < x) && (x < b) then StrictlyInside
    else if x = b then HigherEnd
    else Above

let is_inside x i =
    let p = pos_interval x i in
    if p = LowerEnd || p = StrictlyInside || p = HigherEnd || p = OnlyElement 
    then true else false

let is_above x i = (pos_interval x i = Above)

let is_below x i = (pos_interval x i = Below)

let is_contained (a, b) i2 =
    (* checks whether [a, b] is contained within i2 *)
    if is_inside a i2 && is_inside b i2 then true else false

(* * * * SET CONSTRUCTORS AND AUXILLIARY FUNCTIONS * * * *)

let height = function
    | Node (_, _, _, h, _) -> h
    | Empty -> 0

let size = function
    | Node (_, _, _, _, s) -> s
    | Empty -> 0

let parent_height l r = 
    (* calculates height of the tree given its subtrees *)
    max (height l) (height r) + 1

let parent_size l (a, b) r =
    (* calculates size of the tree given its subtrees and its root *)
    (size l) + (size r) + (b - a) + 1

let empty = Empty

let is_empty s = (s = Empty)

let make l k r = Node(l, k, r, (parent_height l r), (parent_size l k r))

let interval_node i = make empty i empty

let bal s =
    (* almost untouched from pSet *)
    match s with
    | Node(l, k, r, _, _) ->
      let hl = height l in
      let hr = height r in
      if hl > hr + 2 then
        match l with
        | Node (ll, lk, lr, _, _) ->
            if height ll >= height lr then make ll lk (make lr k r)
            else
              (match lr with
              | Node (lrl, lrk, lrr, _, _) ->
                  make (make ll lk lrl) lrk (make lrr k r)
              | Empty -> assert false)
        | Empty -> assert false
      else if hr > hl + 2 then
        match r with
        | Node (rl, rk, rr, _, _) ->
            if height rr >= height rl then make (make l k rl) rk rr
            else
              (match rl with
              | Node (rll, rlk, rlr, _, _) ->
                  make (make l k rll) rlk (make rlr rk rr)
              | Empty -> assert false)
        | Empty -> assert false
      else Node (l, k, r, max hl hr + 1, (parent_size l k r))
    | Empty -> Empty

let rec min_elt = function
    | Node (Empty, k, _, _, _) -> k
    | Node (l, _, _, _, _) -> min_elt l
    | Empty -> raise Not_found

let rec remove_min_elt = function
    | Node (Empty, _, r, _, _) -> r
    | Node (l, k, r, _, _) -> bal (make (remove_min_elt l) k r)
    | Empty -> invalid_arg "ISet.remove_min_elt"

let rec max_elt = function
    | Node (_, k, Empty, _, _) -> k
    | Node (_, _, r, _, _) -> max_elt r
    | Empty -> raise Not_found

let rec remove_max_elt = function
    | Node (l, _, Empty, _, _) -> l
    | Node (l, k, r, _, _) -> bal (make l k (remove_max_elt r))
    | Empty -> invalid_arg "ISet.remove_max_elt"
    
let rec merge t1 t2 =
    (* we only use merge as the return values for remove, so we are sure that
       t1 and t2 have no equal or neighbouring numbers;
       it's modified from the original version in pSet so that 
       it returns a balanced tree *)
    match t1, t2 with
    | Empty, _ -> t2
    | _, Empty -> t1
    | Node(l1, i1, r1, h1, _), Node(l2, i2, r2, h2, _) ->
        if (abs (h1 - h2)) < 2 then
            (* copy-pasted from pSet.ml *)
            let k, nt2 = (min_elt t2), (remove_min_elt t2) in
            make t1 k nt2
        else if h1 > h2 then
            (* we are looking for a place where left and right tree are of
               similar height; otherwise our merge would be unbalanced *)
            if is_empty r1 then
                (* h1 = 3 i h2 = 1, since height r1 = 0 so height l1 <= 2,
                   therefore h1 <= 3, but also h1 - h2 >= 2 => h1 >= 2 + h2,
                   h2 > 0; thus h1 = 3 & h2 = 1; since t2 is a single interval
                   we can just put it in place of empty r1 *)
                make l1 i1 t2 else
            let nl, ni = (remove_max_elt t1), (max_elt t1) in
            merge nl (bal (make empty ni t2))
        else (* h1 < h2 *)
            if is_empty l2 then
                (* same reasoning as above - symmetry *)
               make t1 i2 r2 else
            let nr, ni = (remove_min_elt t2), (min_elt t2) in
            merge (bal (make t1 ni empty)) nr

(* * * * OPERATIONS ON SETS * * * *)

let all_ints = (min_int, max_int)

let rec remove (a, b) s =
    assert (a <= b);
    if (a, b) = all_ints then empty else
    match s with
    | Node(l, i, r, _, _) -> (
        let a_low, b_low, a_high, b_high = 
            (* protection from int overflow *)
            (if a = min_int then a else a - 1),
            (if b = min_int then b else b - 1),
            (if a = max_int then a else a + 1),
            (if b = max_int then b else b + 1) in
        bal (
        let pa, pb = (pos_interval a i), (pos_interval b i) in
        match pa with
        (* we check where do the ends of the interval fall within the tree
           to proceed further on a case-by-case basis *)
        | OnlyElement -> (
            match pb with
            | OnlyElement ->
                merge l r
            | Below | LowerEnd | StrictlyInside | HigherEnd ->
                assert false;
            | Above ->
                merge l (remove (min_int, b) r)
            )
        | Below -> (
            match pb with
            | Below -> 
                make (remove (a, b) l) i r
            | LowerEnd | StrictlyInside -> 
                make (remove (a, max_int) l) (b_high, snd i) r
            | HigherEnd | OnlyElement -> 
                (* i is cut off fully *)
                merge (remove (a, max_int) l) r
            | Above ->
                merge (remove (a, max_int) l) (remove (min_int, b) r)
            )
        (* from this point l will be left alone, because we now know
           no number from [a, b] is in the left subtree *)
        | LowerEnd -> (
            match pb with
            | OnlyElement | Below -> assert false;
            | LowerEnd | StrictlyInside ->
                make l (b_high, snd i) r
            | HigherEnd ->
                merge l r
            | Above ->
                merge l (remove (min_int, b) r) 
            )               
        | StrictlyInside -> (
            match pb with
            | OnlyElement | Below | LowerEnd -> assert false;
            | StrictlyInside ->
                merge
                    (make l (fst i, a_low) empty)
                    (make empty (b_high, snd i) r)
            | HigherEnd ->
                merge
                    (make l (fst i, a_low) empty)
                    r
            | Above ->
                merge
                    (make l (fst i, a_low) empty)
                    (remove (min_int, b) r)
            )
        | HigherEnd ->
            make l (fst i, a_low) (remove (min_int, b) r)
        | Above ->
            make l i (remove (a, b) r)
        ))
    | Empty -> Empty

let subset_above x s =
    remove (min_int, x) s

let subset_below x s =
    remove (x, max_int) s

let rec find x s =
    (* this functions finds where in a tree is x and returns a tuple
       ((integral containing x), (true if x is in the set false otherwise)) *)
    match s with
    | Node(l, i, r, _, _) ->
        if is_inside x i then (i, true) else
        if is_above x i then find x r else
        find x l
    | Empty ->
        (invalid_integral, false)

let containing_interval x s = fst (find x s)

let mem x s = snd (find x s)

let rec add (a, b) s = 
    assert (a <= b);
    match s with
    | Node(l, i, r, _, _) ->
        let a1, b1 = 
            (* protection from int overflow *)
            (if a = min_int then a else a - 1),
            (if b = max_int then b else b + 1) in
        if is_above a1 i then
            bal (make l i (add (a, b) r))
        else if is_below b1 i then
            bal (make (add (a, b) l) i r)
        else
            let (c, d) =
                let (pl, ml), (pr, mr) = find a1 s, find b1 s in
                match ml, mr with
                | (true, true) ->
                    (fst pl, snd pr)
                | (true, false) ->
                    (fst pl, b)
                | (false, true) ->
                    (a, snd pr)
                | (false, false) ->
                    (a, b)
            in 
            let nl, nr = subset_below c s, subset_above d s in
            make nl (c, d) nr
    | Empty ->
        interval_node (a, b)

let iter f set =
  let rec loop = function
    | Empty -> ()
    | Node (l, k, r, _, _) -> loop l; f k; loop r in
  loop set

let fold f set acc =
  let rec loop acc = function
    | Empty -> acc
    | Node (l, k, r, _, _) ->
          loop (f k (loop acc l)) r in
  loop acc set

let rec elements s =
    match s with
    | Node(l, i, r, _, _) ->
        (elements l)@(i::(elements r))
    | Empty ->
        []

let all = make empty (min_int, max_int) empty

let rec below x s =
    if (x = max_int && s = all) then max_int else
    let sum =
        match s with
        | Node(l, (a, b), r, _, _) -> (
            match pos_interval x (a, b) with
            | Below -> below x l
            | Above -> (size l) + (b - a) + 1 + (below x r)
            | _ -> (size l) + (x - a) + 1 )
        | Empty -> 0 in
    if (sum < 0) then max_int else sum

let split x s =
    ((remove (x, max_int) s), (mem x s), (remove (min_int, x) s))
